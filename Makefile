CMSSW ?= CMSSW_12_4_0
GITLAB ?= https://gitlab.cern.ch
ORIGIN ?= $(GITLAB)/DasAnalysisSystem/Core.git
BASE ?= $(PWD)
TABLES ?= $(GITLAB)/DasAnalysisSystem/tables.git

.PHONY: all clean

.NOTPARALLEL: all

all: $(CMSSW) libgit2 TUnfold ProtoDarwin tables
	cd $(CMSSW) && \
	eval `scramv1 runtime -sh` && \
	scram b

TUnfold: $(CMSSW)
	wget https://www.desy.de/~sschmitt/TUnfold/TUnfold_V17.9.tgz
	mkdir -p TUnfold TUnfold/lib
	mv TUnfold_V17.9.tgz TUnfold
	cd TUnfold && tar xvzf TUnfold_V17.9.tgz
	cd $(CMSSW) && eval `scramv1 runtime -sh` && \
	cd $(BASE)/TUnfold && make lib TUNFOLDVERSION='V17' -j && \
	mv -f libunfold.so lib/libtunfold.so && \
	mv -f TUnfoldV17Dict_rdict.pcm lib/ && \
	cd $(BASE)/$(CMSSW) && scram setup $(BASE)/tunfold.xml

PlottingHelper: $(CMSSW)
	git clone $(GITLAB)/DasAnalysisSystem/PlottingHelper.git
	cd $(CMSSW) && eval `scramv1 runtime -sh` && cd - && \
    make -C PlottingHelper all -j2

KinFitter: $(CMSSW)
	# TODO

FastNLO:
	# TODO

# TODO: improve make statement & better interface with content of xml file (using `xmllint` --> check availability in CI)
ProtoDarwin: $(CMSSW) libgit2 tables
	git clone $(GITLAB)/paconnor/ProtoDarwin.git
	cd $(CMSSW) && eval `scramv1 runtime -sh` && \
	BOOST=$$(scram tool tag boost_header BOOSTHEADER_BASE) && \
	cd $(BASE)/ProtoDarwin && make -j BOOST=$$BOOST && \
	cd $(BASE)/$(CMSSW) && scram setup $(BASE)/protodarwin.xml

libgit2: $(CMSSW)
	cd $(BASE)/$(CMSSW) && eval `scramv1 runtime -sh` && scram setup $(BASE)/libgit2.xml

$(CMSSW):
	scramv1 project CMSSW $(CMSSW)
	cd $(CMSSW)/src && eval `scramv1 runtime -sh` && \
	git clone https://github.com/cms-jet/JetToolbox.git JMEAnalysis/JetToolbox -b jetToolbox_120X && \
	git clone $(ORIGIN) Core

tables:
	git clone $(TABLES) $@

clean:
	@rm -rf $(CMSSW) TUnfold ProtoDarwin tables
